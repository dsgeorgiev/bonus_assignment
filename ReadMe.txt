Bricks Game - dgv500/2622131

I have used the skeletoon formt eh snake game in order to imlement another
classic game Bricks. 

The provided skeleton has been changed to suit the purposes of the new game. 
In order to change the size of the screen you can change the DefaultRows variable in
the logic.BricksLogic class.

The gameplay is classic and when you win/lose the game you can restart it using the R key.

I have tried to create sounds for when the bal is hitting a brick or the slider but I got
several erros so i removed this functionality. You can still see the Custom audio class, where
I am reading wav files from my personal site and playing them in the game. For now only the
theme music is on. 

