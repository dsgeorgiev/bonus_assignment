package bricks.logic

import bricks.game.{BlueBrick, DarkGreenBrick, Empty, GrayBrick, GreenBrick, GridType, LawnGreenBrick, LightBlueBrick, OrangeBrick, PurpleBrick, RedBrick, SliderBlock, YellowBrick}
import bricks.logic.BricksLogic.{DefaultColumns, DefaultRows}

import scala.collection.mutable.ArrayBuffer
import scala.util.Random

class BoardHelper {
  var bricks : ArrayBuffer[Brick] = _
  var slider : Slider = _
  var ball : Ball = _
  var themeMusic : CustomAudio = new ThemeMusic()


  def init(nrColumns : Int): Unit = {
    bricks = new ArrayBuffer[Brick]()
    for (i <- 1 until nrColumns - 1  by 2) {
      for (j <- 2 until 12) {
        val brick : Brick = new Brick(i, j, getRandomBrick())
        brick.init()
        bricks.append(brick)
      }
    }
    slider = new Slider(DefaultColumns/2-3, DefaultRows-1, SliderBlock())
    slider.init()
    ball = new Ball(slider.x+4,DefaultRows-3)

    themeMusic.play()
  }

  def getBrickType(x : Int, y : Int): GridType ={
    var gridType : GridType = Empty()
    for(i <- bricks.indices){
      if (bricks(i).getX() == x && bricks(i).getY() == y && bricks(i).brickType != Empty()) {
        gridType = bricks(i).brickType
      }
    }
    gridType
  }

  def contains(point: Point): Boolean ={
    var contains : Boolean = false
    for(i <- bricks.indices){
      if (bricks(i).getX() == point.x && bricks(i).getY() == point.y && bricks(i).brickType != Empty()) {
        contains = true
      }
    }
    contains
  }

  def changeBrickType(point: Point): Unit ={
    for(i <- bricks.indices){
      if(bricks(i).contains(point.x,point.y)){
        bricks(i).brickType = Empty()
      }
    }
  }


  def getRandomBrick() : GridType = {

    val randomGen : Random = new Random()
    val colorIndex = randomGen.nextInt(10)
    colorIndex match {
      case 0 =>
        PurpleBrick()
      case 1 =>
        LawnGreenBrick()
      case 2 =>
        DarkGreenBrick()
      case 3 =>
        BlueBrick()
      case 4 =>
        GrayBrick()
      case 5 =>
        RedBrick()
      case 6 =>
        LightBlueBrick()
      case 7 =>
        YellowBrick()
      case 8 =>
        OrangeBrick()
      case 9 =>
        GreenBrick()
    }
  }
}
