package bricks.logic

import engine.random.{RandomGenerator, ScalaRandomGen}
import bricks.game.{BallType, Direction, Empty, GridType, Idle, Left, Right, SliderBlock}
import bricks.logic.BricksLogic._

class BricksLogic(val randomGen: RandomGenerator,
                  val nrColumns: Int,
                  val nrRows: Int) {

  def this() = this(new ScalaRandomGen(), DefaultColumns, DefaultRows )

  var score = 0
  var gameInPorgress = true
  var board = new BoardHelper()
  board.init(DefaultColumns)

  var keyDown = false

  def isGameOver(): Boolean = {
    if(board.ball.getY() == DefaultRows-1){
      board.themeMusic.stop()
      gameInPorgress = false
      true
    }else{
      false
    }
  }

  def isGameWinner() : Boolean = {
    var gameWinner : Boolean = true
    for(i <- board.bricks.indices){
      if(board.bricks(i).brickType != Empty()){
        gameWinner = false
      }
    }

    if(gameWinner){
      board.themeMusic.stop()
      gameInPorgress = false
    }

    gameWinner
  }

  def step(): Unit = {
    if(!isGameOver() && !isGameWinner()) {
      moveSlider()
      updateBoard()

      if(!checkForCollision()) {
        board.ball.x += board.ball.moveX
      }

      if (board.ball.getX() > DefaultColumns - 2 || board.ball.getX() < 1) {
        board.ball.moveX *= -1
      }

      if (board.ball.getY() < 0 || board.slider.contains(board.ball.x, board.ball.y+1)) {
        board.ball.moveY *= -1
      }

      if(!checkForCollision()) {
        board.ball.y -= board.ball.moveY
      }
    }
  }

  def moveSlider(): Unit ={
    if(keyDown) {
      board.slider.direction match {
        case Right() =>
          board.slider.moveRight(DefaultColumns)
        case Left() =>
          board.slider.moveLeft()
        case Idle() =>
      }
    }
  }

  def changeSliderDirection(direction : Direction, keyPressed : Boolean): Unit ={
    if(!isGameOver()) {
      direction match {
        case Right() =>
          board.slider.direction = Right()
        case Left() =>
          board.slider.direction = Left()
        case Idle() =>
          board.slider.direction = Idle()
      }
      keyDown = keyPressed
    }
  }

  def updateBoard(): Unit ={
    if(getGridTypeAt(board.ball.x - 1, board.ball.y) != Empty() && getGridTypeAt(board.ball.x - 1, board.ball.y) != SliderBlock()){
      board.changeBrickType(new Point(board.ball.x - 1, board.ball.y))
      board.ball.moveX *= -1
      score += 100
      //board.brickSound.play()
    }else if(getGridTypeAt(board.ball.x + 1, board.ball.y) != Empty() && getGridTypeAt(board.ball.x + 1, board.ball.y) != SliderBlock()){
      board.changeBrickType(new Point(board.ball.x + 1, board.ball.y))
      board.ball.moveX *= -1
      score += 100
      //board.brickSound.play()
    }
    if(getGridTypeAt(board.ball.x, board.ball.y - 1) != Empty() && getGridTypeAt(board.ball.x, board.ball.y - 1) != SliderBlock()){
      board.changeBrickType(new Point(board.ball.x, board.ball.y - 1))
      board.ball.moveY *= -1
      score += 100
      //board.brickSound.play()
    }else if(getGridTypeAt(board.ball.x, board.ball.y + 1) != Empty() && getGridTypeAt(board.ball.x, board.ball.y + 1) != SliderBlock()){
      board.changeBrickType(new Point(board.ball.x, board.ball.y + 1))
      board.ball.moveY *= -1
      score += 100
      //board.brickSound.play()
    }
  }

  def checkForCollision(): Boolean ={
    var cillision = false
    if(getGridTypeAt(board.ball.x - 1, board.ball.y) != Empty() && getGridTypeAt(board.ball.x - 1, board.ball.y) != SliderBlock()){
      cillision = true
    }else if(getGridTypeAt(board.ball.x + 1, board.ball.y) != Empty() && getGridTypeAt(board.ball.x + 1, board.ball.y) != SliderBlock()){
      cillision = true
    }

    if(getGridTypeAt(board.ball.x, board.ball.y - 1) != Empty() && getGridTypeAt(board.ball.x, board.ball.y - 1) != SliderBlock()){
      cillision = true
    }else if(getGridTypeAt(board.ball.x, board.ball.y + 1) != Empty() && getGridTypeAt(board.ball.x, board.ball.y + 1) != SliderBlock()){
      cillision = true
    }

    cillision
  }

  def resetGame(): Unit = {
    board.init(DefaultColumns)
  }

  def getGridTypeAt(x: Int, y: Int): GridType = {
    if(board.contains(new Point(x,y))){
      board.getBrickType(x, y)
    }else if(board.slider.contains(x,y)){
      SliderBlock()
    }else if(board.ball.contains(x,y)) {
      BallType()
    }else {
      Empty()
    }
  }
}

object BricksLogic {
  val DefaultColumns = 32
  val DefaultRows = 50
}