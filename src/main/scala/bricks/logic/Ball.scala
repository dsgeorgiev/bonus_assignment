package bricks.logic

import java.awt.Rectangle

class Ball(posX : Int, posY : Int) extends Rectangle{
  var moveX : Int = 1
  var moveY : Int = 1

  this.x = posX
  this.y = posY
  this.width = 1
  this.height = 1
}

