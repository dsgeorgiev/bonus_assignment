package bricks.logic

import java.awt.Rectangle

import bricks.game.GridType
class Brick (var posX : Int, var posY : Int, var brickType : GridType) extends Rectangle{

  def init(): Unit ={
    this.x = posX
    this.y = posY
    this.width = 2
    this.height = 1
  }
}

