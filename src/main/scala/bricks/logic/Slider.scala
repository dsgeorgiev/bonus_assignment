package bricks.logic

import java.awt.Rectangle

import bricks.game.{Direction, GridType, Idle}

import scala.collection.mutable.ArrayBuffer

class Slider (var posX : Int, posY : Int, var brickType : GridType) extends Rectangle{
  var direction : Direction = Idle()

  def init(): Unit ={
    this.x = posX
    this.y = posY
    this.width = 7
    this.height = 1
  }

  def moveLeft(): Unit ={
    if(posX != 0){
      posX -= 1
    }
    this.x = posX
  }

  def moveRight(nrCoulumns: Int): Unit ={
    if((posX + this.width) != nrCoulumns){
      posX += 1
    }
    this.x = posX
  }
}
