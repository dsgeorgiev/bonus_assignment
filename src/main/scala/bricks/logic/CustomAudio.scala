package bricks.logic

import java.net.URL

import javax.sound.sampled.{AudioInputStream, AudioSystem, Clip}

abstract class CustomAudio {

  def play(): Unit = { }
  def stop(): Unit = { }
}

class ThemeMusic() extends CustomAudio{

  val clip = AudioSystem.getClip

  override def play(): Unit ={

    val url = new URL("https://dsgeorgiev.com/music_game/game_theme.wav")
    val audioIn = AudioSystem.getAudioInputStream(url)

    clip.open(audioIn)
    clip.loop(5)
    clip.start

  }

  override def stop(): Unit = {
    clip.close()
  }
}

class WinSound() extends CustomAudio{

  val clip = AudioSystem.getClip
  override def play(): Unit ={

    val url = new URL("https://dsgeorgiev.com/music_game/game_winner.wav")
    val audioIn = AudioSystem.getAudioInputStream(url)

    clip.open(audioIn)
    clip.start
  }

  override def stop(): Unit = {
    clip.close()
  }
}

class BrickSound() extends CustomAudio{
  val clip = AudioSystem.getClip
  override def play(): Unit ={

    val url = new URL("https://dsgeorgiev.com/music_game/brick.wav")
    val audioIn = AudioSystem.getAudioInputStream(url)
    clip.open(audioIn)
    clip.start
  }

  override def stop(): Unit = {
    clip.close()
  }
}

class SliderSound() extends CustomAudio{
  val clip = AudioSystem.getClip

  override def play(): Unit ={
    val url = new URL("https://dsgeorgiev.com/music_game/slider.wav")
    val audioIn = AudioSystem.getAudioInputStream(url)
    clip.open(audioIn)
    clip.start
  }

  override def stop(): Unit = {
    clip.close()
  }
}

class GameOverMusic() extends CustomAudio{
  val clip = AudioSystem.getClip

  override def play(): Unit ={

    val url = new URL("https://dsgeorgiev.com/music_game/brick.wav")
    val audioIn = AudioSystem.getAudioInputStream(url)
    clip.open(audioIn)
    clip.start
  }

  override def stop(): Unit = {
    clip.close()
  }
}
