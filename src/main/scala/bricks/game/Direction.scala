// DO NOT MODIFY FOR BASIC SUBMISSION
// scalastyle:off

package bricks.game

sealed abstract class Direction {
//  def opposite : Direction
//  def next : Direction
}

case class Left()  extends Direction  { }
case class Right()  extends Direction  { }
case class Idle()  extends Direction  { }


