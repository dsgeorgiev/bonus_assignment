
package bricks.game

sealed abstract class GridType

case class Empty() extends GridType
case class BallType() extends GridType
case class SliderBlock() extends GridType

case class PurpleBrick() extends GridType
case class LawnGreenBrick() extends GridType
case class DarkGreenBrick() extends GridType
case class BlueBrick() extends GridType
case class GrayBrick() extends GridType
case class RedBrick() extends GridType
case class LightBlueBrick() extends GridType
case class YellowBrick() extends GridType
case class OrangeBrick() extends GridType
case class GreenBrick() extends GridType
