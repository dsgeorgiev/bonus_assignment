// DO NOT MODIFY FOR BASIC SUBMISSION
// scalastyle:off

package bricks.game

import java.awt.event

import processing.core.{PApplet, PConstants}
import processing.event.KeyEvent
import java.awt.event.KeyEvent._

import engine.GameBase
import engine.graphics.{Color, Point, Rectangle}
import bricks.game.BricksGame._
import engine.graphics.Color._
import bricks.logic.{BricksLogic, CustomAudio, GameOverMusic, WinSound}


class BricksGame extends GameBase {

  var gameLogic = new BricksLogic()
  val updateTimer = new UpdateTimer(FramesPerSecond)
  val widthInPixels: Int = WidthCellInPixels * gameLogic.nrColumns
  val heightInPixels: Int = HeightCellInPixels * gameLogic.nrRows
  val screenArea: Rectangle = Rectangle(Point(0, 0), widthInPixels, heightInPixels)
  var winnerSound : CustomAudio = new WinSound()
  var looserSound : CustomAudio = new GameOverMusic()

  // this function is wrongly named draw by processing (is called on each update next to drawing)
  override def draw(): Unit = {
    updateState()
    drawGrid()
    if (gameLogic.isGameOver){
      //looserSound.play()
      drawGameOverScreen()
    }
    if (gameLogic.isGameWinner) {
      //winnerSound.play()
      drawGameWinnerScreen()
    }

    if(gameLogic.gameInPorgress){
      drawGameScore()
    }
  }

  def drawGameOverScreen(): Unit = {
    setFillColor(Red)
    drawTextCentered("GAME OVER!\nYOUR SCORE IS: " + gameLogic.score.toString + "\nBETTER LUCK NEXT TIME!\nPRESS \"R\" TO TRY AGAIN", 20, screenArea.center)
  }

  def drawGameWinnerScreen(): Unit ={
    setFillColor(Green)
    drawTextCentered("WINNER, WINNER CHICKEN DINNER :):):)\nKEEP UP THE GOOD WORK!\nPRESS \"R\" TO TRY WIN AGAIN", 20, screenArea.center)
  }

  def drawGameScore(): Unit ={
    setFillColor(Green)
    drawTextCentered("YOUR SCORE IS: " + gameLogic.score.toString, 20, screenArea.center)
  }

  def drawGrid(): Unit = {

    val widthPerCell = screenArea.width / gameLogic.nrColumns
    val heightPerCell = screenArea.height / gameLogic.nrRows

    def getCell(colIndex: Int, rowIndex: Int): Rectangle = {
      val leftUp = Point(screenArea.left + colIndex * widthPerCell,
        screenArea.top + rowIndex * heightPerCell)
      Rectangle(leftUp, widthPerCell, heightPerCell)
    }

    def drawCell(area: Rectangle, cell: GridType): Unit = {
      cell match {
        case BallType() =>
          setFillColor(Color.Red)
          drawEllipse(area)
        case SliderBlock()  =>
          setFillColor(Color.Black)
          drawSlider(area)
        case PurpleBrick() =>
          setFillColor(Color.Purple)
          drawBrick(area)
        case LawnGreenBrick() =>
          setFillColor(Color.LawnGreen)
          drawBrick(area)
        case DarkGreenBrick() =>
          setFillColor(Color.DarkGreen)
          drawBrick(area)
        case BlueBrick() =>
          setFillColor(Color.Blue)
          drawBrick(area)
        case GrayBrick() =>
          setFillColor(Color.Gray)
          drawBrick(area)
        case RedBrick() =>
          setFillColor(Color.Red)
          drawBrick(area)
        case LightBlueBrick() =>
          setFillColor(Color.LightBlue)
          drawBrick(area)
        case YellowBrick() =>
          setFillColor(Color.Yellow)
          drawBrick(area)
        case OrangeBrick() =>
          setFillColor(Color.Orange)
          drawBrick(area)
        case GreenBrick() =>
          setFillColor(Color.Green)
          drawBrick(area)
        case Empty() => ()
      }
    }

    setFillColor(White)
    drawRectangle(screenArea)

    for (y <- 0 until gameLogic.nrRows;
         x <- 0 until gameLogic.nrColumns) {
      drawCell(getCell(x, y), gameLogic.getGridTypeAt(x, y))
    }

  }

  /** Method that calls handlers for different key press events.
    * You may add extra functionality for other keys here.
    * See [[event.KeyEvent]] for all defined keycodes.
    *
    * @param event The key press event to handle
    */
  override def keyPressed(event: KeyEvent): Unit = {

    def changeSliderDirection(direction : Direction, keyPressed : Boolean): Unit = gameLogic.changeSliderDirection(direction, keyPressed)
    def resetGame(): Unit = gameLogic.resetGame()

    event.getKeyCode match {
      case VK_RIGHT  =>
        changeSliderDirection(Right(), true)
      case VK_LEFT  =>
        changeSliderDirection(Left(), true)
      case VK_R =>
        if(gameLogic.isGameOver || gameLogic.isGameWinner()) {
          resetGame()
        }
      case _        => ()
    }
  }

  override def keyReleased(event: KeyEvent): Unit = {

    def changeSliderDirection(direction : Direction, keyPressed : Boolean): Unit = gameLogic.changeSliderDirection(direction, keyPressed)

    event.getKeyCode match {
      case VK_RIGHT  =>
        changeSliderDirection(Right(), false)
      case VK_LEFT  =>
        changeSliderDirection(Left(), false)
      case _        => ()
    }
  }



  override def settings(): Unit = {
    pixelDensity(displayDensity())
    size(widthInPixels, heightInPixels, PConstants.P2D)
  }

  override def setup(): Unit = {
    // Fonts are loaded lazily, so when we call text()
    // for the first time, there is significant lag.
    // This prevents it from happening during gameplay.
    text("", 0, 0)
    // This should be called last, since the game
    // clock is officially ticking at this point
    updateTimer.init()
  }


  def updateState(): Unit = {
    if (updateTimer.timeForNextFrame()) {
      gameLogic.step()
      updateTimer.advanceFrame()
    }
  }

}


object BricksGame {

  val FramesPerSecond: Int = 15
  val WidthCellInPixels: Int = 20
  val HeightCellInPixels: Int = WidthCellInPixels

  def main(args: Array[String]): Unit = {
    // This is needed for Processing, using the name
    // of the class in a string is not very beautiful...
     PApplet.main("bricks.game.BricksGame")

  }

}
